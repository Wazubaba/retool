// Copyright (c) 2017 Wazubaba
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

module util;

import std.getopt;
import std.stdio;

struct ProjectMap
{
	string[] dsrc; /// Target source directories to scan
	string dinc; /// Target directory to output headers to
	string ddoc; /// Target directory to output documentation to
	string[] ddep; /// Target include directories to link
	bool buildHeaders = true;
	bool buildDocumentation = true;

	void fillInBlanks(string defaultSrc = "src", string defaultInc = "include", string defaultDoc = "doc")
	{
		if (dsrc.length == 0) dsrc ~= defaultSrc;
		if (buildHeaders && dinc.length == 0) dinc = defaultInc;
		if (buildDocumentation && ddoc.length == 0) ddoc = defaultDoc;
	}

	string toString()
	{
		import std.conv: to;
		string rhs;
		rhs ~= "Source Directories:    " ~ to!string(dsrc);
		if (buildHeaders) rhs ~= "\nHeader Output Directory:    " ~ to!string(dinc);
		if (buildDocumentation) rhs ~= "\nDocumentation Output Directory:    " ~ to!string(ddoc);
		rhs ~= "\nDependency Directories:    " ~ to!string(ddep);

		return rhs;
	}
}

const enum HELP =
"Usage: %s [OPTION]...

Mandatory arguments to long options are mandatory for short options too.

-d, --documentation-dir=DIR     - directory to output documentation to. Does
                                  nothing if -D is used

-h, --header-dir=DIR            - directory to output headers to. Does nothing
                                  if -H is used

-s, --source-dir                - source directory to append to build

-p, --project                   - instruct %s to consider all following args
                                  as part of a seperate project.

-q, --quiet                     - do not output logging information

-D, --no-build-documentation    - do not build documentation files for this
                                  project

-H, --no-build-headers          - do not build header files for this project

-I, --include=DIR               - Adds a directory to use as a dependency for
                                  building docs or headers

Note: if args are ommitted then by default both docs and headers will be
created. The default paths are
* source: ./src
* include output: ./include
* documentation output: ./doc

-p can be specified as many times as needed.";

ProjectMap[] handleArgs(ref string[] args, string defaultSrc = "src", string defaultInc = "include", string defaultDoc = "doc")
{
	ProjectMap[] rhs;

	// Don't do all the arg processing if not needed, just assume single project
	if (args.length <= 1)
	{
		ProjectMap single;
		single.fillInBlanks();
		return [single];
	}

	string[][] projects;

	debug (phase) writeln("===PHASE 1===");

	string[] cluster;
	// Start off assuming a first project.
	foreach (arg; args)
	{
		debug(test)
		{
			writefln("'%s'", arg);
			writefln("\t%s = -p : %s", arg, arg.length > 1 && arg[0..2] == "-p");
			writefln("\t%s = %s : %s", arg, "--project"[0..9], arg.length > 8 && arg[0..9] == "--project");
		}
		if ((arg.length > 1 && arg[0..2] == "-p") || (arg.length > 8 && arg[0..9] == "--project"))
		{
			projects ~= cluster;
			cluster = ["./retool"];
		}
		else
			cluster ~= arg;
	}

	projects ~= cluster;

	debug(projectCount) writefln("Total projects discovered: %s", projects.length);

	debug (phase) writeln("===PHASE 2===");

	foreach (project; projects)
	{
//		writeln(project);
		bool dummy;
		bool noDocs = false;
		bool noHeaders = false;

		ProjectMap pmap;
		auto argInfo = getopt(
			project,
			std.getopt.config.caseSensitive,
//			std.getopt.config.bundling, // <- this breaks stuff with getopt...
			"include|I", "Adds a directory to use as a dependency for building docs/headers", &pmap.ddep,
			"no-build-headers|H", "Do not build header files for this project", &noHeaders,
			"no-build-documentation|D", "Do not build documentation files for this project", &noDocs,
			"header-dir|h", "Directory to output headers to. Does nothing if -H is used.", &pmap.dinc,
			"documentation-dir|d", "Directory to output documentation to. Does nothing if -D is used.", &pmap.ddoc,
			"source-dir|s", "Source directory to append to build", &pmap.dsrc,
			"project|p", "Instruct retool to consider all following args as part of a seperate project.", &dummy
		);

		// getopt sets bools to true if they exist. IDK how to invert this behaviour other than like this :/
		if (noDocs) pmap.buildDocumentation = false;
		if (noHeaders) pmap.buildHeaders = false;

		pmap.fillInBlanks(defaultSrc, defaultInc, defaultDoc);
		rhs ~= pmap;
		pmap = pmap.init;
	}

	return rhs;
}

unittest
{
	import std.stdio;
	string[] TESTARGS = ["./retool", "-H", "-D", "-p", "-htaco", "-ddogs", "--project", "--no-build-documentation", "--header-dir=nya", "-smeow", "-p", "-ssrc", "--source-dir", "test", "-sfix", "-Isupport/lib1/include", "--include", "support/lib2/include"];
	writeln("Test projects:");
	writeln("\t==First==");
	writeln("\t\tDo not build docs or headers\n");
	writeln("\t==Second==");
	writeln("\t\tHeader dir: taco, Doc dir: dogs\n");
	writeln("\t==Third==");
	writeln("\t\tDo not build docs");
	writeln("\t\tHeader dir: nya, Src dir: meow\n");
	writeln("\t==Fourth++");
	writeln("\t\tDo not build includes");
	writeln("\t\tSrc dirs: src, test, fix, Dep dirs: support/lib1/include, support/lib2/include");

	writeln("===============================");

	foreach (i, project; handleArgs(TESTARGS))
	{
		writeln("Project ", i);
		writeln(project);
		writeln("================");
	}
}
