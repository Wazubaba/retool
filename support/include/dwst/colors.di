// D import file generated from 'src/colors.d'
module dwst.colors;
import std.string : isNumeric;
import std.conv : to;
const string NULLCOLOR = "\x1b[0m";
const string RESETCOLORS = "\x1b[39;49m";
const ubyte BLACK = 0;
const ubyte RED = 1;
const ubyte GREEN = 2;
const ubyte YELLOW = 3;
const ubyte BLUE = 4;
const ubyte MAGENTA = 5;
const ubyte CYAN = 6;
const ubyte WHITE = 7;
ubyte customColor(ubyte r, ubyte g, ubyte b);
struct Color
{
	ubyte color;
	bool isBold;
	bool isDim;
	bool isItalic;
	bool isUnderlined;
	bool isBlinking;
	bool isFastBlinking;
	bool isReversed;
	bool isInvisible;
	bool isBackground;
	bool isCrossedout;
	string opCast(T : string)()
	{
		string rhs = "\x1b[";
		if (isBold)
			rhs ~= "1;";
		if (isDim)
			rhs ~= "2;";
		if (isItalic)
			rhs ~= "3;";
		if (isUnderlined)
			rhs ~= "4;";
		if (isBlinking)
			rhs ~= "5;";
		if (isFastBlinking)
			rhs ~= "6;";
		if (isReversed)
			rhs ~= "7;";
		if (isInvisible)
			rhs ~= "8;";
		if (isCrossedout)
			rhs ~= "9;";
		if (this.color == 0)
		{
			rhs = rhs[0..$ - 1];
			rhs ~= "m";
		}
		else
		{
			if (this.color < 8)
			{
				if (isBackground)
					rhs ~= "4";
				else
					rhs ~= "3";
			}
			else
			{
				if (isBackground)
					rhs ~= "48;5;";
				else
					rhs ~= "38;5;";
			}
			rhs ~= to!string(color) ~ "m";
		}
		debug (1)
		{
			import std.stdio : writeln;
			writeln("escape code generated: ", rhs[1..$]);
		}

		return rhs;
	}
}
string colorize(string, S...)(string input, S args)
{
	string rhs;
	foreach (arg; args)
	{
		rhs ~= cast(string)arg;
	}
	rhs ~= input ~ NULLCOLOR ~ RESETCOLORS;
	return rhs;
}
