// Copyright (c) 2017 Wazubaba
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import std.stdio: writeln, writefln;
import std.path: buildPath;
import std.file: exists, isDir;

import scan;
import util;

import dwst.colors;

const enum SOURCEDEFAULT = "src";
const enum DOCUMENTATIONDEFAULT = "doc";
const enum INCLUDEDEFAULT = "include";
const enum DEFAULTCOMPILER = "ldc2";

int main(string[] args)
{
	foreach (arg; args)
	{
		if (arg == "-h" || arg == "--help")
		{
			writefln(HELP, args[0], args[0]);
			return 0;
		}
	}

	Color normalColor = {color: CYAN, isBold: true};

	auto maps = handleArgs(args, SOURCEDEFAULT, INCLUDEDEFAULT, DOCUMENTATIONDEFAULT);

	writeln("R E T O O L".colorize(normalColor));
	writeln("===========");

	foreach (i, project; maps)
	{
		DocMap dmap;
		IncMap imap;

		writeln("Scanning project ", i+1);
		writeln(project);

		foreach (srcDir; project.dsrc)
		{
			if (!srcDir.exists || !srcDir.isDir)
			{
				writeln("ERROR:".colorize(CREDBOLD), " Target source directory '", srcDir.colorize(CYELLOWBOLD), "' does not exist!");
				return 1;
			}

			writefln("Scanning '%s/' for source files...", srcDir);
			foreach (srcFile; findDSourceInDir(srcDir))
			{
				string modname = srcFile.getModulePath;
				if (modname !is null)
				{
					writefln("\tAdded '%s'", srcFile);
					dmap[srcFile] = buildPath(project.ddoc, modname);
					imap[srcFile] = buildPath(project.dinc, modname);
				}
			}
		}

		if (project.buildDocumentation)
		{
			writefln("Building docs to '%s/'...", project.ddoc);
			foreach (path, outName; dmap)
				if ((buildDocs(DEFAULTCOMPILER, path, outName, project.ddep)) != 0)
					writeln("ERROR:".colorize(CREDBOLD), " Cannot build docs for '" ~ path ~ "'");
		}

		if (project.buildHeaders)
		{
			writefln("Building includes to '%s/'...", project.dinc);
			foreach (path, outName; imap)
				if ((buildIncludes(DEFAULTCOMPILER, path, outName, project.ddep)) != 0)
					writeln("ERROR:".colorize(CREDBOLD), " Cannot build includes for '" ~ path ~ "'");
		}
	}
	return 0;
}
