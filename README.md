# R E T O O L
`retool` assists in building documentation and headers for various projects
in the D programming language.
.
The goal was to make something that is easy to use, had no need for meta
configuration files, and could be easilly wired into an existing workflow.

It relies on ldc2 by default, but in the future may support per-project
compiler choice and a master override for it. Currently you can change the
default settings by editing `src/main.d` around lines 33-36 and rebuilding.

## Usage
`retool` is inspired by the unix `find` command's syntax, though aims to be a
bit less complex.

Every command is handled as if it were for a single project. This behaviour
changes however when you use the `-p` or `--project` flag. Following this flag
all future arguments are considered a part of an entirely seperate project.
In this way, one can generate documentation and headers for any number of
projects.

## Future
Currently `retool` is only designed to support linux. I frankly have no drive
to support windows and no way of supporting osx, but at some point will OCD
into it most likely. To do this will require you to aquire and build with the
same compiler as you did `retool` my colors library, and then update the
Makefile to properly link it in. At some point I'll get around to making this
an optional dependency or something, as the colors lib does not function
properly on windows outside of a real shell such as cygwin or putty anyways...
