// Copyright (c) 2017 Wazubaba
//
// MIT License
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

module scan;

import std.stdio;
import std.string;
import std.path;
import std.process;
import std.file;
import std.regex;

import dwst.colors;

alias DocMap = string[string];
alias IncMap = string[string];

enum MODULEREG = ctRegex!("[^\"]*module*[^\"][^\"*module*\"]");

Color CMAGENTABOLD = {color: MAGENTA, isBold: true};
Color CGREENBOLD = {color: GREEN, isBold: true};
Color CBLUE = {color: BLUE};
Color CYELLOWBOLD = {color: YELLOW, isBold: true};
Color CREDBOLD = {color: RED, isBold: true};

string[] findDSourceInDir(string path)
{
	string[] rhs;
	foreach (DirEntry node; dirEntries(path, SpanMode.depth))
	{
		if (node.isFile && (node.name.extension == ".d" || node.name.extension == ".di"))
			rhs ~= node.name;
	}

	return rhs;
}

string getModulePath(string path)
{
	auto fp = File(path, "r");
	string rhs;

	string line;
	while ((line = fp.readln()) !is null)
	{
		auto result = line.matchFirst(MODULEREG);
		if (!result.empty)
		{
			string lastWord = "";
			foreach(i, word; line.split())
			{
				if (lastWord == "module")
				{
					rhs = word;
					break;
				}
				lastWord = word;
			}

			if (rhs.length > 0)
			{
				if (rhs[$-1] == ';') rhs = rhs[0..$-1];
				break;
			}

			string fixed;
			foreach (ch; rhs)
			{
				switch(ch)
				{
					case '.': fixed ~= '/'; break;
					default: fixed ~= ch; break;
				}
			}
			rhs = fixed;
		}
	}
	fp.close();
	return rhs.replace(".", "/");//dirSeparator);
}

uint buildDocs(string compiler, string path, string outName, string[] dependencyDirs = [])
{
	string[] cmd = [compiler, "-c", "-o-", "-D"];
	cmd ~= "-Df" ~ outName ~ ".html";
	if (dependencyDirs.length > 0)
	{
		foreach (depDir; dependencyDirs) cmd ~= "-I%s".format(depDir);
	}

	cmd ~= path;

	writefln("\t%s ".colorize(CBLUE) ~ "→".colorize(CGREENBOLD) ~ " %s.html".colorize(CYELLOWBOLD) ~ ":" ~  "%s".colorize(CMAGENTABOLD), path, outName, cmd);
	auto rhs = execute(cmd);
	write(rhs.output);
	return rhs.status;
}

uint buildIncludes(string compiler, string path, string outName, string[] dependencyDirs = [])
{
	string[] cmd = [compiler, "-c", "-o-", "-H"];
	cmd ~= "-Hf" ~ outName ~ ".di";
	if (dependencyDirs.length > 0)
	{
		foreach (depDir; dependencyDirs) cmd ~= "-I%s".format(depDir);
	}

	cmd ~= path;

	writefln("\t%s ".colorize(CBLUE) ~ "→".colorize(CGREENBOLD) ~ " %s.di".colorize(CYELLOWBOLD) ~ ":" ~  "%s".colorize(CMAGENTABOLD), path, outName, cmd);
	auto rhs = execute(cmd);
	write(rhs.output);
	return rhs.status;
}
