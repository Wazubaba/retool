DC= ldc2
BIN= retool
INCLUDE= -Isupport/include
LIBS= support/lib

ifeq ($(shell uname -m), x86_64)
	LIBFILES= $(shell find $(LIBS)/lin64 -type f -name "*.a")
else
	# Assume we are on 32bit or something
	LIBFILES= $(shell find $(LIBS)/lin32 -type f -name "*.a")
endif

# Allow overriding of the arch
ifeq ($(ARCH), 32)
	LIBFILES= $(shell find $(LIBS)/lin32 -type f -name "*.a")
endif

ifeq ($(ARCH), 64)
	LIBFILES= $(shell find $(LIBS)/lin64 -type f -name "*.a")
endif

ifndef ($DFLAGS)
ifeq ($(DEBUG), yes)
	DFLAGS= -w -d-debug=phase -d-debug=projectCount -unittest -gc #-sanitize=address
else
	DFLAGS= -release -Oz
endif
endif

SRCFILES= $(shell find src -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
OBJFILES= $(patsubst %.d, .bldfiles/%.o, $(SRCFILES))

all: $(BIN)

.bldfiles/%.o: %.d
	$(DC) $(DFLAGS) -Isrc $(INCLUDE) -c -of$@ $<

$(BIN): $(OBJFILES)
	$(DC) $(DFLAGS) -of$@ $(OBJFILES) $(LIBFILES)

clean:
	-@$(RM) $(BIN)
	-@$(RM) -r .bldfiles

shrinkBinary: $(BIN)
	-strip --strip-all $(BIN)
	-upx --best --ultra-brute $(BIN)
